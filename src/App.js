import React from 'react'
import './App.css'


let result = ''
class App extends React.Component{
    state = {info: ''}

    changeState = () => {
        this.setState({
            info: result
        })
    }

    analyzer(){
        let date = new Date();
        let day = date.getDay()
        
        
        switch(day){
            case 0: 
                result="6 days more for Party";
                break;
            case 1:
                 result="5 days more for Party"
                 break;
            case 2: 
                 result="4 days more for Party";
                 break;
            case 3:
                 result="3 days more for Party";
                 break;
            case 4:
                 result="2 days more for Party";
                 break;
            case 5: 
                result="1 days more for Party";
                break;
            default: 
                result= "Today is the day!";
                break;
        }
        
        this.changeState();


    }



    render(){
        console.log(this.state.info)
        return(
            <div className="center">
                <div className=" ui cards">
                <div className="card">
                    <div className="content">
                        <div className="header"> Curious to know when is the Next Weekend? </div>
                        <div className="description">{this.state.info} </div>

                    </div>
                    <div className="ui bottom attached button" onClick={()=>{this.analyzer()}}>
                    Click here to revel!
                    </div>
                   
                </div>

            
            </div>

            </div>
            
        );
    }
}

export default App